﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rolo.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NewRolo.Tests;
using Rolo.Models;
using System;

namespace NewRolo.Tests
{
    [TestClass()]
    public class AdminServiceTests
    {
        private AdminService service;
        private MockDataContext db = new MockDataContext();

        [TestInitialize]
        public void Initialize()
        {
            ApplicationUser bjoggi = new ApplicationUser { Id = "1", Name = "Bjorgvin Litli", UserName = "bjoggib", Email = "bla@bla.is", DateOfBirth = DateTime.Now, ProfilePic = new Image { Id = 1, Path = "http://andri.pedro.is/images/bjoggib.jpg" } };
            ApplicationUser vissihaus = new ApplicationUser { Id = "2", Name = "Vissi Hauksson", UserName = "vissihaus", Email = "vissi@haus.is", DateOfBirth = DateTime.Now, ProfilePic = new Image { Id = 2, Path = "http://andri.pedro.is/images/vissihaus.jpg" } };
            ApplicationUser andri = new ApplicationUser { Id = "3", Name = "Andri Mar", UserName = "andri", Email = "blabla@bla.is", DateOfBirth = DateTime.Now, ProfilePic = new Image { Id = 3, Path = "http://andri.pedro.is/images/andri.jpg" } };
            ApplicationUser doctorinn = new ApplicationUser { Id = "4", Name = "Asgeir Thor", UserName = "doctorinn", Email = "doctor@doctor.is", DateOfBirth = DateTime.Now, ProfilePic = new Image { Id = 4, Path = "http://andri.pedro.is/images/doctorinn.jpg" } };

            db.Users.Add(bjoggi);
            db.Users.Add(vissihaus);
            db.Users.Add(andri);
            db.Users.Add(doctorinn);

            FriendConnection fc1 = new FriendConnection { Id = 1, UserId = bjoggi, FriendId = vissihaus, Accepted = true };
            FriendConnection fc2 = new FriendConnection { Id = 2, UserId = andri, FriendId = bjoggi, Accepted = true };
            FriendConnection fc3 = new FriendConnection { Id = 3, UserId = andri, FriendId = vissihaus, Accepted = false };
            db.FriendConnections.Add(fc1);
            db.FriendConnections.Add(fc2);
            db.FriendConnections.Add(fc3);

            var playgroundList = new List<Playground>
            {
                new Playground {
                    Id=1,
                    Name="Freyjugöturóló",
                    Features=new PlaygroundFeature {InfantSwings=true, Swings=true, Sandbox=false, InfantSlide=false, Slide=true, InfantJungleGym=true, JungleGym=true, SeeSaw=false, RockingHorse=true, BalancingEquipment=true, Goal=false, Basket=false, PlayHouse=true},
                    Image= new Image{Path="http://andri.pedro.is/images/rolo_freyjugoturolo.jpg"}, 
                    Latitude=64.142618,
                    Longitude=-21.930805,
                },
                new Playground {
                    Id=2,
                    Name="Barónsborg",
                    Features=new PlaygroundFeature {InfantSwings=false, Swings=true, Sandbox=true, InfantSlide=false, Slide=true, InfantJungleGym=false, JungleGym=true, SeeSaw=true, RockingHorse=false, BalancingEquipment=false, Goal=false, Basket=true, PlayHouse=true},
                    Image= new Image {Path="http://andri.pedro.is/images/rolo_baronsborg.jpg"}, 
                    Latitude=64.142231,
                    Longitude= -21.920037,
                },
                new Playground {
                    Id=3,
                    Name="Njálsborg",
                    Features=new PlaygroundFeature {InfantSwings=false, Swings=true, Sandbox=true, InfantSlide=false, Slide=true, InfantJungleGym=false, JungleGym=true, SeeSaw=false, RockingHorse=true, BalancingEquipment=false, Goal=false, Basket=true, PlayHouse=true},
                    Image= new Image {Path="http://andri.pedro.is/images/rolo_njalsborg.jpg"},
                    Latitude=64.144541,
                    Longitude=-21.928298,
                },
                new Playground {
                    Id=4,
                    Name="Grænuborg",
                    Features=new PlaygroundFeature {InfantSwings=false, Swings=true, Sandbox=true, InfantSlide=false, Slide=true, InfantJungleGym=false, JungleGym=true, SeeSaw=false, RockingHorse=false, BalancingEquipment=false, Goal=false, Basket=false, PlayHouse=true},
                    Image= new Image {Path="http://andri.pedro.is/images/rolo_graenuborg.jpg"}, 
                    Latitude=64.141172,
                    Longitude=-21.928093,
                },
                new Playground {
                    Id=5,
                    Name="Austurbæjarskóli",
                    Features=new PlaygroundFeature {InfantSwings=false, Swings=true, Sandbox=false, InfantSlide=false, Slide=true, InfantJungleGym=false, JungleGym=true, SeeSaw=false, RockingHorse=false, BalancingEquipment=false, Goal=true, Basket=true, PlayHouse=false},
                    Image= new Image {Path="http://andri.pedro.is/images/rolo_austurbaejarskoli.jpg"}, 
                    Latitude=64.142129,
                    Longitude=-21.922389,
                }
            };

            playgroundList.ForEach(s => db.Playgrounds.Add(s));

            CheckInConnection cc1 = new CheckInConnection { Id = 1, UserId = bjoggi, PlaygroundId = db.Playgrounds.Where(x => x.Id == 1).Single(), DateCreated = DateTime.Now };
            CheckInConnection cc2 = new CheckInConnection { Id = 2, UserId = vissihaus, PlaygroundId = db.Playgrounds.Where(x => x.Id == 5).Single(), DateCreated = DateTime.Now };
            db.CheckInConnections.Add(cc1);
            db.CheckInConnections.Add(cc2);

            PlaygroundConnection pc1 = new PlaygroundConnection { Id = 1, UserId = andri, PlaygroundId = db.Playgrounds.Where(x => x.Id == 2).Single() };
            PlaygroundConnection pc2 = new PlaygroundConnection { Id = 2, UserId = vissihaus, PlaygroundId = db.Playgrounds.Where(x => x.Id == 4).Single() };
            db.PlaygroundConnections.Add(pc1);
            db.PlaygroundConnections.Add(pc2);

            PlaygroundRating pr1 = new PlaygroundRating { Id = 1, UserId = andri, PlaygroundId = db.Playgrounds.Where(x => x.Id == 1).Single(), Rating = 5 };
            PlaygroundRating pr2 = new PlaygroundRating { Id = 2, UserId = bjoggi, PlaygroundId = db.Playgrounds.Where(x => x.Id == 2).Single(), Rating = 1 };
            PlaygroundRating pr3 = new PlaygroundRating { Id = 3, UserId = vissihaus, PlaygroundId = db.Playgrounds.Where(x => x.Id == 1).Single(), Rating = 3 };
            db.PlaygroundRatings.Add(pr1);
            db.PlaygroundRatings.Add(pr2);
            db.PlaygroundRatings.Add(pr3);

            db.SaveChanges();

            service = new AdminService(db);
        }

        [TestMethod()]
        public void GetAllPlaygroundsTest()
        {
            // Arrange:
            // Act:
            service.GetAllPlaygrounds();
            // Assert:
            Assert.AreEqual(5, db.Playgrounds.ToList().Count);
        }

        [TestMethod()]
        public void RemovePlaygroundWithIdTest()
        {
            // Arrange:
            int playgroundId = 1;
            // Act:
            service.RemovePlaygroundWithId(playgroundId);
            // Assert:
            Assert.AreEqual(4, db.Playgrounds.ToList().Count);
        }

        [TestMethod()]
        public void GetPlaygroundWithIdTest()
        {
            // Arrange:
            int playgroundId = 1;
            // Act:
            var result = service.GetPlaygroundWithId(playgroundId);
            // Assert:
            Assert.AreEqual("Freyjugöturóló", result.Name);
        }

        [TestMethod()]
        public void AddPlaygroundTest()
        {
            // Arrange:
            Playground p = new Playground
            {
                Id = 6,
                Name = "Vissaróló",
                Features = new PlaygroundFeature { InfantSwings = false, Swings = false, Sandbox = false, InfantSlide = false, Slide = false, InfantJungleGym = false, JungleGym = false, SeeSaw = false, RockingHorse = false, BalancingEquipment = false, Goal = false, Basket = false, PlayHouse = false },
                Image = new Image { Path = "http://andri.pedro.is/images/rolo_graenuborg.jpg" },
                Latitude = 66.0,
                Longitude = -66.0,
            };
            // Act:
            service.AddPlayground(p);
            // Assert:
            Assert.AreEqual(6, db.Playgrounds.ToList().Count);
        }
    }
}
