﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Rolo.Startup))]
namespace Rolo
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
