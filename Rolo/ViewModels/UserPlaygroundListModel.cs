﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Rolo.ViewModels
{
    public class UserPlaygroundListModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public bool ActiveFriends { get; set; }
        public bool CurrentUserIsCheckedIn { get; set; }
        public bool Empty { get; set; }
        public int Range { get; set; }
    }
}