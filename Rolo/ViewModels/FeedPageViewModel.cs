﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Rolo.ViewModels;
using Rolo.Models;

namespace Rolo.ViewModels
{
	public class FeedPageViewModel
	{
        public List<PostViewModel> Feed { get; set; }
	}
}