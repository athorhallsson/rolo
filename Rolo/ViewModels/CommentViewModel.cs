﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Rolo.ViewModels;
using Rolo.Models;

namespace Rolo.ViewModels
{
	public class CommentViewModel
	{
        public int Id { get; set; }
        public string Text { get; set; }
        public UserListViewModel Author { get; set; }
        public DateTime DateCreated { get; set; }
	}
}