﻿using Rolo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Rolo.ViewModels
{
    public class MePageViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public Image ProfilePic { get; set; }
        public DateTime DateOfBirth { get; set; }
    }
}