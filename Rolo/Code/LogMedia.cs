﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Rolo.Code
{
    public abstract class LogMedia
    {
        public abstract void LogMessage(string message);
    }
}