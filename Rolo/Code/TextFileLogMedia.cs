﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace Rolo.Code
{
    public class TextFileLogMedia : LogMedia
    {
        public override void LogMessage(string message)
        {
            string path = ConfigurationManager.AppSettings["LogFilePath"];

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            string fullPath = path + "/" + ConfigurationManager.AppSettings["Username"] + "_logfile.txt";

            try
            {
                using (StreamWriter writer = new StreamWriter(fullPath, true, Encoding.Default))
                {
                    writer.WriteLine(message);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(DateTime.Now);
                Debug.WriteLine(ex.Message);
                Debug.WriteLine(ex.StackTrace);
            }
        }
    }
}