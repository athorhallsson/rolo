﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Rolo.Code
{
    public class Logger
    {
        private List<LogMedia> loggers = new List<LogMedia>();

        private static Logger theInstance = null;

        private Logger()
        {
            loggers.Add(new EmailLogMedia());
            loggers.Add(new TextFileLogMedia());
            loggers.Add(new OutputWindowLogMedia());
        }

        public static Logger Instance
        {
            get
            {
                if (theInstance == null)
                {
                    theInstance = new Logger();
                }
                return theInstance;
            }
        }

        public void LogException(Exception ex)
        {
            string errorMessage = String.Format("The following error occured: \r\n Timespamp: {0} \r\n Message: {1} \r\n StackTrace: {2}", DateTime.Now, ex.Message, ex.StackTrace);

            foreach (var item in loggers)
            {
                item.LogMessage(errorMessage);
            }
        }
    }
}