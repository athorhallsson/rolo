﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace Rolo.Code
{
    public class EmailLogMedia : LogMedia
    {
        public override void LogMessage(string message)
        {
            string fromEmail = ConfigurationManager.AppSettings["FromEmail"];
            string errorHandlingEmail = ConfigurationManager.AppSettings["ErrorHandlingEmail"];

            try
            {
                using (MailMessage emailMessage = new MailMessage())
                {
                    emailMessage.To.Add(errorHandlingEmail);
                    emailMessage.Subject = "An error occured.";
                    emailMessage.Body = message;
                    using (SmtpClient client = new SmtpClient())
                    {
                        client.EnableSsl = true;
                        client.Send(emailMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(DateTime.Now);
                Debug.WriteLine(ex.Message);
                Debug.WriteLine(ex.StackTrace);
            }
        }
    }
}