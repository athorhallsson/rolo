﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace Rolo.Code
{
    public class OutputWindowLogMedia : LogMedia
    {
        public override void LogMessage(string message)
        {
            Debug.WriteLine(message);
        }
    }
}