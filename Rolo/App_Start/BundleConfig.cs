﻿using System.Web;
using System.Web.Optimization;

namespace Rolo
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                        "~/Scripts/bootstrap.js",
                        "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                        "~/Content/bootstrap.css",
                        "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/PlaygroundListScript").Include(
                        "~/Scripts/PlaygroundListScript.js"));

			bundles.Add(new ScriptBundle("~/bundles/UserMyPlaygroundListScript").Include(
						"~/Scripts/UserMyPlaygroundListScript.js"));

            bundles.Add(new ScriptBundle("~/bundles/AddUserPost").Include(
                        "~/Scripts/AddUserPost.js"));

            bundles.Add(new ScriptBundle("~/bundles/SearchUsers").Include(
                        "~/Scripts/SearchUsers.js"));

			bundles.Add(new ScriptBundle("~/bundles/addPlaygroundPost").Include(
						"~/Scripts/addPlaygroundPost.js"));

			bundles.Add(new ScriptBundle("~/bundles/notifications").Include(
						"~/Scripts/notifications.js"));

			bundles.Add(new ScriptBundle("~/bundles/timeAgo").Include(
						"~/Scripts/timeAgo.js"));

            bundles.Add(new ScriptBundle("~/bundles/SearchPlaygrounds").Include(
                        "~/Scripts/SearchPlaygrounds.js"));

            bundles.Add(new ScriptBundle("~/bundles/UserSearchPlayground").Include(
                        "~/Scripts/UserSearchPlayground.js"));

            bundles.Add(new StyleBundle("~/Content/AdminCss").Include(
                        "~/Content/Admin.css"));

            bundles.Add(new ScriptBundle("~/bundles/playgroundpage").Include(
                        "~/Scripts/playgroundpage.js"));
           
            bundles.Add(new ScriptBundle("~/bundles/gmaps").Include(
                    "~/Scripts/gmaps.js"));

            bundles.Add(new ScriptBundle("~/bundles/friendpage").Include(
                    "~/Scripts/friendpage.js"));

			bundles.Add(new ScriptBundle("~/bundles/friendSprite").Include(
					"~/Scripts/friendSprite.js"));

			bundles.Add(new ScriptBundle("~/bundles/meSprites").Include(
					"~/Scripts/meSprites.js"));

            bundles.Add(new ScriptBundle("~/bundles/combodate").Include(
                    "~/Scripts/combodate.js"));

            bundles.Add(new ScriptBundle("~/bundles/moment").Include(
                    "~/Scripts/moment.js"));
        }
    }
}
