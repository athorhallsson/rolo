﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using Rolo.Models;
using Rolo.ViewModels;
using Rolo.DAL;
using Rolo.Services;
using System.Diagnostics;
using Newtonsoft.Json;
using System.Globalization;

namespace Rolo.Controllers
{
    public class HomeController : ExceptionController
    {
        private PlaygroundService pService = new PlaygroundService(null);
		private UserService uService = new UserService(null);
        private FeedService fService = new FeedService(null);
        private ConnectionService cService = new ConnectionService(null);

        // Frontpage for unregistered user
		public ActionResult Guest()
		{
			if (Request.IsAuthenticated)
			{
				return RedirectToAction("PlaygroundList");
			}

			return View();
		}

        // Frontpage for registered user
        public ActionResult PlaygroundList()
        {
			if (!Request.IsAuthenticated)
			{
  				return View("Guest");
			}
			
            return View(pService.getPlaygroundListForUser(User.Identity.GetUserId()));
        }

        // Feed to display posts and comments
        [Authorize]
        public ActionResult Feed()
        {
			string userId = User.Identity.GetUserId();
			List<ApplicationUser> friendList = uService.GetAllFriendsForUser(userId);
			List<Playground> playgrounds = pService.GetPlaygroundsForUser(userId);
			var model = fService.GetUserFeedById(friendList, userId, playgrounds);
			return View(model);
        }

        // Displays a list of friends and friendrequests
        [Authorize]
		public ActionResult Friend()
		{
            string userId = System.Web.HttpContext.Current.User.Identity.GetUserId();
            FriendsPageViewModel model = new FriendsPageViewModel();
            model.Friends = uService.GetFriendsViewmodelsForUser(userId);
            model.Requests = uService.GetRequestsViewModelsForUser(userId);
			return View(model);
		}

        // Displays information about the current user
        [Authorize]
		public ActionResult Me()
		{
            string userId = User.Identity.GetUserId();
            var model = uService.GetInfoForMe(userId);
			return View(model);
		}

        // Displays information about a playground with a specific Id and the feed for that playground
		public ActionResult PlaygroundInfo(int? id)
		{
            if (id.HasValue)
            {
                if (!User.Identity.IsAuthenticated)
                {
                    return RedirectToAction("GuestPlaygroundInfo", new { id = id.Value });
                }
				var listOfPosts = fService.GetPostsByPlaygrId(id.Value);
                var model = pService.GetPlaygroundInfo(id.Value, User.Identity.GetUserId(), listOfPosts);

                if (model != null)
                {
                    return View(model);
                }
            }
            return View("Error");
		}

        // Displays limited information about a playground with a specific Id for an unregistered user
        public ActionResult GuestPlaygroundInfo(int? id)
        {
            if (id.HasValue)
            {
                PlaygroundViewModel model = pService.GetGuestPlaygroundViewModel(id.Value);
                return View(model);
            }
            return View("Error");
        }

        // Displays information about a user with a specific username
        [Authorize]
        public ActionResult UserPage(string username)
        {
            if (String.IsNullOrEmpty(username))
            {
                return View("Error");
            }
            UserPageViewModel model = new UserPageViewModel();
            model = uService.GetInfoForUser(username);
            string userId = User.Identity.GetUserId();
            model.hasPendingRequestFromUser = cService.CheckPendingRequest(userId, username);
            model.didSendFriendRequest = cService.CheckFriendRequestSent(userId, username);
            model.areFriends = cService.CheckIfFriends(userId, username);
            return View(model);
        }

        // Adds a comment to the database and returns the updated feed
		[HttpPost]
		public ActionResult AddCommentToPostUserJson(FormCollection collection)
		{
			string commentText = collection["comment-input"];
			string stringPostId = collection["post-id"];
			int postId = Convert.ToInt32(stringPostId);
			string userId = User.Identity.GetUserId();
			fService.AddComment(commentText, postId, userId);

			List<ApplicationUser> friendList = uService.GetAllFriendsForUser(userId);
			List<Playground> playgrounds = pService.GetPlaygroundsForUser(userId);
			var model = fService.GetUserFeedById(friendList, userId, playgrounds);
			return Json(model, JsonRequestBehavior.AllowGet);
		}

        // Adds a comment on a playground feed to the database and returns the updated feed
		[HttpPost]
		public ActionResult AddCommentToPostPlaygrJson(FormCollection collection)
		{
			string commentText = collection["comment-input"];
			string stringPostId = collection["post-id"];
			string playgrId = collection["playgr-id"];
			int postId = Convert.ToInt32(stringPostId);
			int playgroundId = Convert.ToInt32(playgrId);
			string userId = System.Web.HttpContext.Current.User.Identity.GetUserId();
			fService.AddComment(commentText, postId, userId);

			var model = fService.GetPostsByPlaygrId(playgroundId);
			return Json(model, JsonRequestBehavior.AllowGet);
		}

        // Adds a post to the database and returns the updated feed
        [HttpPost]
        public ActionResult AddPostToUserJson(FormCollection collection)
        {
            string postText = collection["new-user-post"];
            string userId = User.Identity.GetUserId();
            fService.AddPost(postText, userId, null);

            List<ApplicationUser> friendList = uService.GetAllFriendsForUser(userId);
			List<Playground> playgrounds = pService.GetPlaygroundsForUser(userId);
			var model = fService.GetUserFeedById(friendList, userId, playgrounds);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        // Adds a post on a playground feed to the database and returns the updated feed
		[HttpPost]
		public ActionResult AddPostToPlaygroundJson(FormCollection collection)
		{
			string postText = collection["new-playground-post"];
			int playgrId = Convert.ToInt32(collection["playground-id"]);

			string userId = User.Identity.GetUserId();
			fService.AddPost(postText, userId, playgrId);

			var model = fService.GetPostsByPlaygrId(playgrId);
			return Json(model, JsonRequestBehavior.AllowGet);
		}

        // Gets nearby playgrounds for unregistered user
        [HttpPost]
        public ActionResult GuestPlaygroundListJson(string lat, string lon)
        {
            if (lat != null)
            {
                Location guestLocation = new Location();
                guestLocation.Latitude = double.Parse(lat, CultureInfo.InvariantCulture);
                guestLocation.Longitude = double.Parse(lon, CultureInfo.InvariantCulture);

                var guestPlaygroundList = pService.GuestGetAllPlaygrounds(guestLocation);
                return Json(guestPlaygroundList, JsonRequestBehavior.AllowGet);
            }
            return null;
        }

        // Gets nearby playgrounds for registered user
        [HttpPost]
        public ActionResult UserPlaygroundListJson(string lat, string lon)
        {
            if (lat != null)
            {
                Location userLocation = new Location();
                userLocation.Latitude = double.Parse(lat, CultureInfo.InvariantCulture);
                userLocation.Longitude = double.Parse(lon, CultureInfo.InvariantCulture);
                
                var userPlaygroundList = pService.GetNearbyPlaygrounds(userLocation, User.Identity.GetUserId());
                return Json(userPlaygroundList, JsonRequestBehavior.AllowGet);
            }
            return null;
        }

        // Creates a PlaygroundConnection between a specific playground a the current user
        [HttpPost]
        public ActionResult FollowPlaygroundJson(FormCollection collection)
        {
            string playgroundString = collection["playground-id"];
            int playgroundId = int.Parse(playgroundString);
            string userId = User.Identity.GetUserId();
            cService.AddPlaygroundConnection(userId, playgroundId);
            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        // Creates or deletes a CheckInConnection between a specific playground a the current user
        [HttpPost]
        public ActionResult CheckInPlaygroundJson(FormCollection collection)
        {
            string playgroundString = collection["playground-id"];
            int playgroundId = int.Parse(playgroundString);
            string userId = User.Identity.GetUserId();
            cService.AddCheckInConnection(userId, playgroundId);
            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        // Creates a FriendConnection between the current user and the selected user
        [HttpPost]
        public ActionResult AddFriendJson(FormCollection collection)
        {
            string friendId = collection["user-id"];
            string userId = User.Identity.GetUserId();
            cService.AddFriendConnection(userId, friendId);
            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        // Deletes a FriendConnection between the current user and the selected user
        [HttpPost]
        public ActionResult RemoveFriendJson(FormCollection collection)
        {
            string friendId = collection["user-id"];
            string userId = User.Identity.GetUserId();
            cService.RemoveFriendConnection(userId, friendId);
            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        // Approves a previosly made FriendConnection which another user made to the current user
        [HttpPost]
        public ActionResult AcceptFriendJson(string id)
        {
            string friendId = id.Remove(0, 7);
            string userId = User.Identity.GetUserId();
            cService.AcceptFriendConnection(userId, friendId);
            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        // Removes a previosly made FriendConnection which another user made to the current user
        [HttpPost]
        public ActionResult DeclineFriendJson(string id)
        {
            string friendId = id.Remove(0, 7);
            string userId = User.Identity.GetUserId();
            cService.RemoveFriendConnection(userId, friendId);
            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        // Searches for Users which include a specified string
        [HttpPost]
        public ActionResult SearchUsersJson(FormCollection collection)
        {
            string search = collection["search"];
            string userId = User.Identity.GetUserId();

            List<UserListViewModel> requests = new List<UserListViewModel>();
            requests = uService.GetRequestUserViewModelsForSearch(search, userId);
            List<UserListViewModel> friends = new List<UserListViewModel>();
            friends = uService.GetFriendsUserViewModelsForSearch(search, userId);
            List<UserListViewModel> others = new List<UserListViewModel>();
            others = uService.GetOthersUserViewModelsForSearch(search, userId);

            List<List<UserListViewModel>> model = new List<List<UserListViewModel>>();
            model.Add(requests);
            model.Add(friends);
            model.Add(others);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        // Checks how many friend requests the current user has
		public ActionResult FriendNotificationJson()
		{
            if (Request.IsAuthenticated)
            {
                var model = cService.GetFriendNotifications(User.Identity.GetUserId());
                return Json(model, JsonRequestBehavior.AllowGet);
            }
            return Json(0, JsonRequestBehavior.AllowGet);
		}

        // Checks how many unread posts the current user has
		public ActionResult FeedNotificationJson()
		{
            if (Request.IsAuthenticated)
            {
                string userId = User.Identity.GetUserId();
                List<ApplicationUser> friendList = uService.GetAllFriendsForUser(userId);
                List<Playground> playgrounds = pService.GetPlaygroundsForUser(userId);
                int numberOfNewPosts = fService.CheckForNewPosts(friendList, userId, playgrounds);

                return Json(numberOfNewPosts, JsonRequestBehavior.AllowGet);
            }
            return Json(0, JsonRequestBehavior.AllowGet);
		}

        // Saves an image for the user and sets the profile picture
        [HttpPost]
        public ActionResult UploadProfile(HttpPostedFileBase upload)
        {
            if (ModelState.IsValid)
            {
                if (upload != null && upload.ContentLength > 0)
                {
                    string userId = User.Identity.GetUserId();
                    var fileName = userId + "-" +  Path.GetFileName(upload.FileName);
                    var path = AppDomain.CurrentDomain.BaseDirectory + "\\Data\\" + fileName;
                    upload.SaveAs(path);
                        
                    Image profilePic = new Image{ Path = String.Format("/Data/{0}", fileName) };

                    uService.AddImageToUser(profilePic, User.Identity.GetUserId());
                }
            }
            return RedirectToAction("Me");
        }

        // Searches for playgrounds for unregistered users
        [HttpPost]
        public ActionResult GuestSearchPlaygroundsJson(FormCollection collection)
        {
            string search = collection["search"];
            string lat = collection["lat"];
            string lon = collection["lon"];

            Location guestLocation = new Location();
            guestLocation.Latitude = double.Parse(lat, CultureInfo.InvariantCulture);
            guestLocation.Longitude = double.Parse(lon, CultureInfo.InvariantCulture);

            List<GuestPlaygroundListModel> model = new List<GuestPlaygroundListModel>();
            model = pService.GuestGetNearbyPlaygroundViewModelsForSearch(search, guestLocation);


            return Json(model, JsonRequestBehavior.AllowGet);
        }

        // Searches for playgrounds for registered users
        [HttpPost]
        public ActionResult UserSearchPlaygroundsJson(FormCollection collection)
        {
            string search = collection["search"];
            string userId = User.Identity.GetUserId();
            string lat = collection["lat"];
            string lon = collection["lon"];

            Location Location = new Location();
            Location.Latitude = double.Parse(lat, CultureInfo.InvariantCulture);
            Location.Longitude = double.Parse(lon, CultureInfo.InvariantCulture);

            List<UserPlaygroundListModel> myPlaygrounds = new List<UserPlaygroundListModel>();
            List<UserPlaygroundListModel> nearbyPlaygrounds = new List<UserPlaygroundListModel>();
            myPlaygrounds = pService.UserGetMyPlaygroundViewModelsForSearch(search, Location, userId);
            nearbyPlaygrounds = pService.UserGetNearbyPlaygroundViewModelsForSearch(search, Location, User.Identity.GetUserId());

            List<List<UserPlaygroundListModel>> model = new List<List<UserPlaygroundListModel>>();
            model.Add(myPlaygrounds);
            model.Add(nearbyPlaygrounds);

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        // Creates a PlaygroundRating and returns the updated average rating for the playground
        [HttpPost]
        public ActionResult RatePlayground(FormCollection collection)
        {
            string playgroundIdString = collection["playground-id"];
            string userId = User.Identity.GetUserId();
            string ratingString = collection["rateinfo"];

            if (String.IsNullOrEmpty(playgroundIdString) || String.IsNullOrEmpty(ratingString))
            {
                return View("Error");
            }
            int playgroundId = Int32.Parse(playgroundIdString);
            int rating = Int32.Parse(ratingString);
            pService.RatePlayground(userId, playgroundId, rating);
            return Json(pService.GetAverageRatingForPlayground(playgroundId), JsonRequestBehavior.AllowGet);
        }

        // Updates the latest date and time the current user opened the feed to be able to count how many unread posts
		public ActionResult UpdateLatestReadJson()
		{
			var userId = User.Identity.GetUserId();
			uService.UpdateLatestRead(userId);

			return Json(0, JsonRequestBehavior.AllowGet); 
		}
    }
}