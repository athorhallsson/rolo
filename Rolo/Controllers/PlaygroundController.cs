﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Rolo.Models;
using Rolo.DAL;
using Rolo.Services;
using Rolo.ViewModels;
using System.IO;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Rolo.Controllers
{
    [Authorize(Roles = "Administrators")]
    public class PlaygroundController : ExceptionController
    {
        private AdminService aService = new AdminService(null);

        // GET: /Playground/
        public ActionResult Index()
        {
            return View(aService.GetAllPlaygrounds());
        }

        // GET: /Playground/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Playground playground = aService.GetPlaygroundWithId(id.Value);
            if (playground == null)
            {
                return HttpNotFound();
            }
            return View(playground);
        }

        // GET: /Playground/Create
        public ActionResult Create()
        {
            return View(new PlaygroundViewModel());
        }

        // POST: /Playground/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        // [Bind(Include = "Id,Name,Latitude,Longitude")]
       // public ActionResult Create(FormCollection collection, HttpPostedFileBase image, PlaygroundFeature feature)
        public ActionResult Create(PlaygroundViewModel model, HttpPostedFileBase image)
        {
            if (ModelState.IsValid)
            {
                Playground newPlayground = new Playground();
                newPlayground.Name = model.Name;
                newPlayground.Latitude = model.Latitude;
                newPlayground.Longitude = model.Longitude;
                newPlayground.Features = model.Features;

                Image playgroundImage = new Image();
                
                if (image != null && image.ContentLength > 0)
                {
                    string userId = User.Identity.GetUserId();
                    var fileName = Path.GetFileName(image.FileName);
                    var path = AppDomain.CurrentDomain.BaseDirectory + "\\Data\\" + fileName;
                    image.SaveAs(path);
                    newPlayground.Image = new Image { Path = String.Format("/Data/{0}", fileName) };
                }

                aService.AddPlayground(newPlayground);
                return RedirectToAction("Index");
            }
            return View();
        }

        // GET: /Playground/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Playground playground = aService.GetPlaygroundWithId(id.Value);
            if (playground == null)
            {
                return HttpNotFound();
            }
            return View(playground);
        }

        // POST: /Playground/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,Name,Latitude,Longitude")] Playground playground)
        {
            if (ModelState.IsValid)
            {
                ApplicationDbContext db = new ApplicationDbContext();
                db.Entry(playground).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(playground);
        }

        // GET: /Playground/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Playground playground = aService.GetPlaygroundWithId(id.Value);
            if (playground == null)
            {
                return HttpNotFound();
            }
            return View(playground);
        }

        // POST: /Playground/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            aService.RemovePlaygroundWithId(id);
            return RedirectToAction("Index");
        }
    }
}
