﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Rolo.Code;

namespace Rolo.Controllers
{
    [HandleError]
    public class ExceptionController : Controller
    {
        protected override void OnException(ExceptionContext filterContext)
        {
            base.OnException(filterContext);

            Exception ex = filterContext.Exception;
            // Logs exceptions to file, with email and console
            Logger.Instance.LogException(ex);
        }
	}
}