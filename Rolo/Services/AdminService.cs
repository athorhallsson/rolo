﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Rolo.DAL;
using Rolo.Models;

namespace Rolo.Services
{
    public class AdminService
    {
        private readonly IAppDataContext db;

        public AdminService(IAppDataContext dbContext)
        {
            db = dbContext ?? new ApplicationDbContext();
        }

        public List<Playground> GetAllPlaygrounds()
        {
           List<Playground> result = (from p in db.Playgrounds
                    select p).ToList();
            if (result != null) 
            {
                result.OrderBy(p => p.Name);
            }
            return result;
        }

        public void RemovePlaygroundWithId(int id)
        {
            Playground playground = (from p in db.Playgrounds
                                    where p.Id == id
                                    select p).Single();
            db.Playgrounds.Remove(playground);
            db.SaveChanges();
        }

        public Playground GetPlaygroundWithId(int id)
        {
            return (from p in db.Playgrounds
                    where p.Id == id
                    select p).Single();
        }

        public void AddPlayground(Playground p)
        {
            db.Playgrounds.Add(p);
            db.SaveChanges();
        }
    }
}