﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Rolo.DAL;
using Rolo.Models;
using System.Diagnostics;

namespace Rolo.Services
{
    public class ConnectionService
    {
        private readonly IAppDataContext db;

        public ConnectionService(IAppDataContext dbContext)
        {
            db = dbContext ?? new ApplicationDbContext();
        }

        public void AddPlaygroundConnection(string userId, int playgroundId)
        {
            ApplicationUser user = (from u in db.Users
                                    where u.Id == userId
                                    select u).SingleOrDefault();
            Playground playground = (from p in db.Playgrounds
                                     where p.Id == playgroundId
                                     select p).SingleOrDefault();
            if (user != null && playground != null)
            {
                PlaygroundConnection checkPc = (from pc in db.PlaygroundConnections
                                                where pc.UserId.Id == userId && pc.PlaygroundId.Id == playgroundId
                                                select pc).SingleOrDefault();
                if (checkPc == null)
                {
                    db.PlaygroundConnections.Add(new PlaygroundConnection { UserId = user, PlaygroundId = playground });
                }
                else
                {
                    db.PlaygroundConnections.Remove(checkPc);
                }
            }
            db.SaveChanges();
        }

        public void AddCheckInConnection(string userId, int playgroundId)
        {
            ApplicationUser user = (from u in db.Users
                                    where u.Id == userId
                                    select u).SingleOrDefault();
            Playground playground = (from p in db.Playgrounds
                                     where p.Id == playgroundId
                                     select p).SingleOrDefault();
            if (user != null && playground != null)
            {
                CheckInConnection checkCc = (from cc in db.CheckInConnections
                                             where cc.UserId.Id == userId && cc.PlaygroundId.Id == playgroundId
                                             select cc).SingleOrDefault();
                if (checkCc == null)
                {
                    CheckInConnection current = (from cc in db.CheckInConnections
                                                 where cc.UserId.Id == userId
                                                 select cc).SingleOrDefault();
                    if (current != null)
                    {
                        db.CheckInConnections.Remove(current);
                    }
                    db.CheckInConnections.Add(new CheckInConnection { UserId = user, PlaygroundId = playground, DateCreated = DateTime.UtcNow });
                }
                else
                {
                    db.CheckInConnections.Remove(checkCc);
                }
            }
            db.SaveChanges();
        }

        public void AddFriendConnection(string userId, string friendId)
        {
            ApplicationUser user = (from u in db.Users
                                    where u.Id == userId
                                    select u).SingleOrDefault();
            ApplicationUser friend = (from u in db.Users
                                    where u.Id == friendId
                                    select u).SingleOrDefault();
            if (user != null && friend != null)
            {
                FriendConnection checkFc = (from fc in db.FriendConnections
                                            where fc.UserId.Id == userId && fc.FriendId.Id == friendId
                                            select fc).SingleOrDefault();
                if (checkFc == null)
                {
                    db.FriendConnections.Add(new FriendConnection { UserId = user, FriendId = friend, Accepted = false });
                    db.SaveChanges();
                }
            }
        }

        public void RemoveFriendConnection(string userId, string friendId)
        {
            ApplicationUser user = (from u in db.Users
                                    where u.Id == userId
                                    select u).SingleOrDefault();
            ApplicationUser friend = (from u in db.Users
                                      where u.Id == friendId
                                      select u).SingleOrDefault();
            if (user != null && friend != null)
            {
                FriendConnection checkFc = (from fc in db.FriendConnections
                                            where (fc.UserId.Id == userId && fc.FriendId.Id == friendId) || (fc.UserId.Id == friendId && fc.FriendId.Id == userId)
                                            select fc).SingleOrDefault();
                if (checkFc != null)
                {
                    db.FriendConnections.Remove(checkFc);
                    db.SaveChanges();
                }
            }
        }

        public void AcceptFriendConnection(string userId, string friendId)
        {
            ApplicationUser user = (from u in db.Users
                                    where u.Id == userId
                                    select u).SingleOrDefault();
            ApplicationUser friend = (from u in db.Users
                                      where u.Id == friendId
                                      select u).SingleOrDefault();
            if (user != null && friend != null)
            {
                FriendConnection checkFc = (from fc in db.FriendConnections
                                            where fc.UserId.Id == friendId && fc.FriendId.Id == userId
                                            select fc).SingleOrDefault();
                if (checkFc != null)
                {
                    checkFc.Accepted = true;
                    db.SaveChanges();
                }
            }
        }

        public bool CheckPendingRequest(string userId, string friendId)
        {
            FriendConnection checkFc = (from fc in db.FriendConnections
                                        where fc.UserId.UserName == friendId  && fc.FriendId.Id == userId && fc.Accepted == false
                                        select fc).SingleOrDefault();
            return checkFc != null;
        }

        public bool CheckFriendRequestSent(string userId, string friendId)
        {
            FriendConnection checkFc = (from fc in db.FriendConnections
                                        where fc.UserId.Id == userId && fc.FriendId.UserName == friendId && fc.Accepted == false
                                        select fc).SingleOrDefault();
            return checkFc != null;
        }

        public bool CheckIfFriends(string userId, string friendId)
        {
            FriendConnection checkFc = (from fc in db.FriendConnections
                                        where (fc.UserId.Id == userId && fc.FriendId.UserName == friendId && fc.Accepted == true)
                                        || (fc.UserId.UserName == friendId && fc.FriendId.Id == userId && fc.Accepted == true)
                                        select fc).SingleOrDefault();
            return checkFc != null;
        }

		public int GetFriendNotifications(string userId)
		{
			var result = (from c in db.FriendConnections
						  where c.FriendId.Id == userId && c.Accepted == false
						  select c).ToList();

			if (result != null)
			{
				return result.Count;
			}

			return 0;
		}
    }
}