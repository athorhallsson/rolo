﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Rolo.ViewModels;
using Rolo.Models;
using Rolo.DAL;

namespace Rolo.Services
{
    public class FeedService
    {
        private readonly IAppDataContext db;

        public FeedService(IAppDataContext dbContext)
        {
            db = dbContext ?? new ApplicationDbContext();
        }

        public List<PostViewModel> GetUserFeedById(List<ApplicationUser> friendList, string userId, List<Playground> playgrounds)
        {
            List<PostViewModel> postViewModelList = new List<PostViewModel>();
            // gets all posts by all the friends and every comment for each post
            foreach (var friend in friendList)
            {
                List<Post> friendPosts = GetPostsByFriendId(friend.Id);

                foreach (var fp in friendPosts)
                {
					if (fp.PlaygroundId == null)
					{
						postViewModelList.Add(MakePostViewModel(fp, friend));
					}
                }                
            }

            List<Post> myPosts = GetPostsByFriendId(userId);
            var user = (from u in db.Users
                        where u.Id == userId
                        select u).Single();

            foreach (var p in myPosts)
            {
                postViewModelList.Add(MakePostViewModel(p, user));
            }

			foreach (var playgr in playgrounds)
			{
				postViewModelList.AddRange(GetPostsByPlaygrId(playgr.Id));
			}

			List<PostViewModel> result = postViewModelList.GroupBy(x => x.Id).Select(y => y.First()).ToList();

            result = (from p in result
                      orderby p.DateModified descending
                      select p).Take(50).ToList();

            return result;
        }

		public List<PostViewModel> GetPostsByPlaygrId(int playgrId)
		{
			var posts = (from p in db.Posts
                         where p.PlaygroundId != null && p.PlaygroundId.Id == playgrId
						orderby p.DateModified descending
                        select p).Take(20).ToList();

			List<PostViewModel> postViewModelList = new List<PostViewModel>();
			foreach (var p in posts)
			{
				ApplicationUser user = (from u in db.Users
										where u.Id == p.AuthorId.Id
										select u).Single();
				postViewModelList.Add(MakePostViewModel(p, user));
			}

			postViewModelList = (from p in postViewModelList
								 orderby p.DateModified descending
								 select p).ToList();

			List<PostViewModel> result = postViewModelList.GroupBy(x => x.Id).Select(y => y.First()).ToList();

			return result;
		}

        public PostViewModel MakePostViewModel(Post p, ApplicationUser friend)
        {
            PostViewModel result = new PostViewModel();
            UserListViewModel newAuthor = new UserListViewModel();
            newAuthor.Name = friend.Name;
            newAuthor.ProfilePic = friend.ProfilePic;
			newAuthor.Username = friend.UserName;
            result.Author = newAuthor;
            result.DateCreated = p.DateCreated;
			result.DateModified = p.DateModified;
            result.PostImage = p.ImageId;
            result.Text = p.Text;
            result.Id = p.Id;
			if (p.PlaygroundId != null)
			{
				result.PlaygroundId = p.PlaygroundId.Id;
				result.PlaygroundName = p.PlaygroundId.Name;
			}
			else
			{
				result.PlaygroundId = 0;
				result.PlaygroundName = "";
			}
			
            result.Comments = GetCommentViewModelsByPostId(p.Id);
            return result;            
        }

        public List<Post> GetPostsByFriendId(string id)
        {
            var posts = (from p in db.Posts 
                         where p.AuthorId.Id == id
                         orderby p.DateModified descending
                         select p).Take(20).ToList();
            return posts;
        }

        public List<CommentViewModel> GetCommentViewModelsByPostId(int id)
        {
            var comments = (from c in db.Comments
                            where c.PostId.Id == id
                            orderby c.DateCreated ascending
                            select c).ToList();
            List<CommentViewModel> result = new List<CommentViewModel>();

			foreach (var c in comments)
            {
				ApplicationUser user = (from u in db.Users
										where u.Id == c.AuthorId.Id
										select u).Single();

                CommentViewModel viewModel = new CommentViewModel();
				UserListViewModel newAuthor = new UserListViewModel();
				newAuthor.Name = user.Name;
				newAuthor.ProfilePic = user.ProfilePic;
				newAuthor.Username = user.UserName;
				viewModel.Author = newAuthor;
                viewModel.DateCreated = c.DateCreated;
                viewModel.Id = c.Id;
                viewModel.Text = c.Text;
                result.Add(viewModel);
            }
			List<CommentViewModel> resultNoDuplicates = result.GroupBy(x => x.Id).Select(y => y.First()).ToList();
            
			return resultNoDuplicates;
        }

        public void AddPost(string postText, string userId, int? playgrId)
        {
            var user = (from u in db.Users
                        where u.Id == userId
                        select u).Single();
            Post newPost = new Post();
            newPost.AuthorId = user;
            newPost.Text = postText;
			if (playgrId.HasValue)
			{
				Playground playgr = (from p in db.Playgrounds
									 where p.Id == playgrId.Value
									 select p).Single();
				newPost.PlaygroundId = playgr;
			}
			else
			{
				newPost.PlaygroundId = null;
			}
            
            newPost.ImageId = null;
            newPost.DateCreated = DateTime.UtcNow;
            newPost.DateModified = DateTime.UtcNow;

            db.Posts.Add(newPost);
            db.SaveChanges();
        }

		public void AddComment(string commentText, int postId, string userId)
		{
			ApplicationUser user = (from u in db.Users
						where u.Id == userId
						select u).Single();
			Post post = (from p in db.Posts
						where p.Id == postId
						select p).SingleOrDefault();
			if (post != null)
			{
				Comment newComment = new Comment();
                newComment.DateCreated = DateTime.UtcNow;
                post.DateModified = DateTime.UtcNow;
				newComment.AuthorId = user;
				newComment.Text = commentText;
				newComment.PostId = post;
				
				db.Comments.Add(newComment);
				db.SaveChanges();
			}
		}

		public int CheckForNewPosts(List<ApplicationUser> friendList, string userId, List<Playground> playgrounds)
		{
			ApplicationUser user = (from u in db.Users
						            where u.Id == userId
						            select u).Single();

			var model = GetUserFeedById(friendList, userId, playgrounds);
			var result = model.FindAll(PostViewModel => PostViewModel.DateCreated > user.LatestRead);

			if (result != null)
			{
				return result.Count;
			}
			return 0;
		}
    }
}