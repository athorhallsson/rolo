﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Rolo.Models;
using Rolo.ViewModels;
using Rolo.DAL;
using System.Device.Location;

namespace Rolo.Services
{
    public class PlaygroundService
    {
        private readonly IAppDataContext db;

        public PlaygroundService(IAppDataContext dbContext)
        {
            db = dbContext ?? new ApplicationDbContext();
        }

        public List<UserPlaygroundListModel> GetNearbyPlaygrounds(Location userLoc, string userId)
        {
            List<UserPlaygroundListModel> result = new List<UserPlaygroundListModel>();
            List<Playground> playgroundList = (from p in db.Playgrounds
                                               orderby p.Name
                                               select p).Take(10).ToList();
            foreach(Playground p in playgroundList)
            {
                result.Add(MakeUserPlaygroundListModel(p, userLoc, userId));
            }

			result = result.OrderBy(p => p.Range).ToList();
            return result;
        }

        public UserPlaygroundListModel MakeUserPlaygroundListModel(Playground p, Location userLoc, string userId)
        {
            UserPlaygroundListModel result = new UserPlaygroundListModel();
            result.Id = p.Id;
            result.Name = p.Name;
            result.Latitude = p.Latitude;
            result.Longitude = p.Longitude;
            result.Range = (int)CalculateDistance(userLoc, p.Latitude, p.Longitude);
            result.CurrentUserIsCheckedIn = UserIsCheckedInOnPlayground(userId, p.Id);
            result.ActiveFriends = FriendOnPlayground(userId, p.Id);
            result.Empty = NobodyOnPlayground(p.Id);

            return result;
        }

        public bool NobodyOnPlayground(int playgroundId)
        {
            var result = (from cc in db.CheckInConnections
                          where cc.PlaygroundId.Id == playgroundId
                          select cc).ToList();
            return result.Count == 0;
        }

        public bool FriendOnPlayground(string userId, int playgroundId)
        {
            // Finds the users which added the current user
            List<ApplicationUser> friendList = (from friends in db.Users
                                                join friendcon in db.FriendConnections on friends.Id equals friendcon.FriendId.Id
                                                where friendcon.FriendId.Id != userId && friendcon.UserId.Id == userId && friendcon.Accepted == true
                                                orderby friends.Name ascending
                                                select friends).ToList();
            // Finds the users added by the current user
            List<ApplicationUser> friendList2 = (from friends in db.Users
                                                 join friendcon in db.FriendConnections on friends.Id equals friendcon.UserId.Id
                                                 where friendcon.UserId.Id != userId && friendcon.FriendId.Id == userId && friendcon.Accepted == true
                                                 orderby friends.Name ascending
                                                 select friends).ToList();
            friendList.AddRange(friendList2);
            foreach(ApplicationUser user in friendList)
            {
                if (UserIsCheckedInOnPlayground(user.Id, playgroundId) == true)
                {
                    return true;
                }
            }
            return false;
        }

        public bool UserIsCheckedInOnPlayground(string userId, int playgroundId)
        {
            var result = (from cc in db.CheckInConnections
                          where userId == cc.UserId.Id && playgroundId == cc.PlaygroundId.Id
                          select cc).SingleOrDefault();
            return result != null;
        }

        public List<GuestPlaygroundListModel> GuestGetAllPlaygrounds(Location userLoc) 
        {
            List<GuestPlaygroundListModel> result = new List<GuestPlaygroundListModel>();
            List<Playground> playgroundList = (from p in db.Playgrounds
                                               orderby p.Name
                                               select p).ToList();
            foreach (Playground p in playgroundList)
            {
                result.Add(MakeGuestPlaygroundListModel(p, userLoc));
            }
            result = result.OrderBy(p => p.Range).ToList();
            return result;
        }

        private GuestPlaygroundListModel MakeGuestPlaygroundListModel(Playground p, Location userLoc)
        {
            GuestPlaygroundListModel result = new GuestPlaygroundListModel();
            result.Id = p.Id;
            result.Name = p.Name;
            result.Latitude = p.Latitude;
            result.Longitude = p.Longitude;
            result.Range = (int)CalculateDistance(userLoc, p.Latitude, p.Longitude);

            return result;
        }

        public double CalculateDistance(Location userLoc, double playLat, double playLon) 
        {
            var userCoord = new GeoCoordinate(userLoc.Latitude, userLoc.Longitude);
            var playCoord = new GeoCoordinate(playLat, playLon);
 
			return userCoord.GetDistanceTo(playCoord);
        }

		public PlaygroundViewModel GetPlaygroundInfo(int id, string currentUserId, List<PostViewModel> posts)
        {
            Playground playground = db.Playgrounds.Find(id);
            if (playground != null)
            {
                return makePlaygroundViewModel(playground, currentUserId, posts);
            }
            return null;
        }

        public PlaygroundViewModel GetGuestPlaygroundViewModel(int id)
        {
            Playground p = db.Playgrounds.Find(id);
            if (p != null)
            {
                PlaygroundViewModel result = new PlaygroundViewModel();
                result.Id = p.Id;
                result.Name = p.Name;
                result.Latitude = p.Latitude;
                result.Longitude = p.Longitude;
                result.Features = (from features in db.PlaygroundFeatures
                                   where features.Id == p.Features.Id
                                   select features).Single();
                result.PlaygroundImage = (from img in db.Images
                                          where img.Id == p.Image.Id
                                          select img).Single();
                result.Rating = this.GetAverageRatingForPlayground(p.Id);
                result.Posts = null;
                result.CurrentUserIsCheckedIn = false;
                result.CurrentUserIsFollowing = false;
                result.ActiveFriends = null;
                result.CurrentUserRating = null;
                return result;
            }
            return null;
        }

		private PlaygroundViewModel makePlaygroundViewModel(Playground p, string currentUserId, List<PostViewModel> posts)
        {
            PlaygroundViewModel result = new PlaygroundViewModel();
            result.Id = p.Id;
            result.Name = p.Name;
            result.Latitude = p.Latitude;
            result.Longitude = p.Longitude;
			result.Posts = posts;
            result.Features = (from features in db.PlaygroundFeatures
                               where features.Id == p.Features.Id
                               select features).Single();
            result.PlaygroundImage = (from img in db.Images
                                      where img.Id == p.Image.Id
                                      select img).Single();
            result.Rating = this.GetAverageRatingForPlayground(p.Id);
            result.CurrentUserRating = (from pr in db.PlaygroundRatings
                                        where pr.UserId.Id == currentUserId && pr.PlaygroundId.Id == p.Id
                                        select pr).SingleOrDefault();
            if (currentUserId != null)
            {
                ApplicationUser currentUser = (from u in db.Users
                                               where u.Id == currentUserId
                                                select u).Single();
                CheckInConnection currentCheckIn = (from cc in db.CheckInConnections
                                                    where cc.PlaygroundId.Id == p.Id && cc.UserId.Id == currentUser.Id
                                                    select cc).SingleOrDefault();
                if (currentCheckIn != null)
                {
                    result.CurrentUserIsCheckedIn = true;
                }
                else
                {
                    result.CurrentUserIsCheckedIn = false;
                }

                PlaygroundConnection currentPlaygroundFollow = (from pc in db.PlaygroundConnections
                                                                where pc.PlaygroundId.Id == p.Id && pc.UserId.Id == currentUser.Id
                                                                select pc).SingleOrDefault();
                if (currentPlaygroundFollow != null)
                {
                    result.CurrentUserIsFollowing = true;
                }
                else
                {
                    result.CurrentUserIsFollowing = false;
                }
            }
            return result;
        }

		public List<UserPlaygroundListModel> getPlaygroundListForUser(string userId)
		{
			List<UserPlaygroundListModel> result = new List<UserPlaygroundListModel>();
			var playgroundList = GetPlaygroundsForUser(userId);

			foreach (Playground p in playgroundList)
			{
				UserPlaygroundListModel temp = new UserPlaygroundListModel();
				temp.Id = p.Id;
				temp.Name = p.Name;
				temp.Latitude = p.Latitude;
				temp.Longitude = p.Longitude;
				temp.Range = 0;
                temp.CurrentUserIsCheckedIn = UserIsCheckedInOnPlayground(userId, p.Id);
                temp.ActiveFriends = FriendOnPlayground(userId, p.Id);
                temp.Empty = NobodyOnPlayground(p.Id);
				result.Add(temp);
			}
			return result;
		}

        public List<Playground> GetPlaygroundsForUser(string userId)
        {
            List<Playground> playgroundList = (from playground in db.Playgrounds
                                               join pc in db.PlaygroundConnections
                                               on playground.Id equals pc.PlaygroundId.Id
                                               where pc.UserId.Id == userId
                                               select playground).ToList();
			return playgroundList;
        }

        public List<GuestPlaygroundListModel> GuestGetNearbyPlaygroundViewModelsForSearch(string search, Location userLoc)
        {
            List<GuestPlaygroundListModel> result = new List<GuestPlaygroundListModel>();
            List<Playground> playgroundList = (from p in db.Playgrounds
                                               where p.Name.StartsWith(search) || p.Name.Contains(search) || p.Name.EndsWith(search)
                                               orderby p.Name
                                               select p).ToList();
            foreach (Playground p in playgroundList)
            {
                result.Add(MakeGuestPlaygroundListModel(p, userLoc));
            }
            result = result.OrderBy(p => p.Range).ToList();
            return result;
        }

        public List<UserPlaygroundListModel> UserGetNearbyPlaygroundViewModelsForSearch(string search, Location userLoc, string userId)
        {
            List<UserPlaygroundListModel> result = new List<UserPlaygroundListModel>();
            List<Playground> playgroundList = (from p in db.Playgrounds
                                               where p.Name.StartsWith(search) || p.Name.Contains(search) || p.Name.EndsWith(search)
                                               orderby p.Name
                                               select p).ToList();
            foreach (Playground p in playgroundList)
            {
                result.Add(MakeUserPlaygroundListModel(p, userLoc, userId));
            }
            result = result.OrderBy(p => p.Range).ToList();
            return result;
        }

        public List<UserPlaygroundListModel> UserGetMyPlaygroundViewModelsForSearch(string search, Location userLoc, string userId)
        {
            List<UserPlaygroundListModel> result = new List<UserPlaygroundListModel>();
            List<Playground> playgroundList = (from playground in db.Playgrounds
                                               join pc in db.PlaygroundConnections
                                               on playground.Id equals pc.PlaygroundId.Id
                                               where pc.UserId.Id == userId && (playground.Name.StartsWith(search) || playground.Name.Contains(search) || playground.Name.EndsWith(search))
                                               orderby playground.Name
                                               select playground).ToList();
            foreach (Playground p in playgroundList)
            {
                result.Add(MakeUserPlaygroundListModel(p, userLoc, userId));
            }
            return result;
        }

        public void RatePlayground(string userId, int playgroundId, int rating)
        {
            PlaygroundRating checkIfRated = (from pr in db.PlaygroundRatings
                                             where pr.PlaygroundId.Id == playgroundId && pr.UserId.Id == userId
                                             select pr).SingleOrDefault();
            if (checkIfRated != null)
            {
                checkIfRated.Rating = rating;
            }
            else
            {
                Playground playground = (from p in db.Playgrounds
                                         where p.Id == playgroundId
                                         select p).Single();
                ApplicationUser user = (from u in db.Users
                                        where u.Id == userId
                                        select u).Single();
                db.PlaygroundRatings.Add(new PlaygroundRating { UserId = user, PlaygroundId = playground, Rating = rating });
            }
            db.SaveChanges();
        }

        public double GetAverageRatingForPlayground(int playgroundId)
        {
            var allRatings = db.PlaygroundRatings.Where(x => x.PlaygroundId.Id == playgroundId).Select(x => x.Rating).ToList();
            if (allRatings != null && allRatings.Count != 0)
            {
                return Math.Round(allRatings.Average(), 0);
            }
            return 0;
        }
    }
}
