﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Rolo.Models
{
    public class FriendConnection
    {
        public int Id { get; set; }
        public virtual ApplicationUser UserId { get; set; }
        public virtual ApplicationUser FriendId { get; set; }
        public bool Accepted { get; set; }
    }
}