﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Rolo.Models
{
    public class PlaygroundRating
    {
        public int Id { get; set; }
        public virtual Playground PlaygroundId { get; set; }
        public virtual ApplicationUser UserId { get; set; }
        public int Rating { get; set; }
    }
}