﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Rolo.Models
{
    public class Post
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public virtual Playground PlaygroundId { get; set; }
        public virtual ApplicationUser AuthorId { get; set; }
        public virtual Image ImageId { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
    }
}