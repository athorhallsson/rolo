﻿var acceptFriend = function(event) {
    var friendId = event.target.id;
    $.ajax({
        type: 'POST',
        url: "/Home/AcceptFriendJson/",
        data: { id: friendId },
        dataType: 'json',
        success: function () {
            location.reload();
        },
        error: function () {
            alert("An error occured. Please try again later.");
        }
    });
}

var removeFriend = function(event) {
    var friendId = event.target.id;
    $.ajax({
        type: 'POST',
        url: "/Home/DeclineFriendJson/",
        data: { id: friendId },
        dataType: 'json',
        success: function () {
            location.reload();
        },
        error: function () {
            alert("An error occured. Please try again later.");
        }
    });
}

$(".accept-btn").click(acceptFriend);
$(".remove-btn").click(removeFriend);

var UpdateFeedFriend = function (dataFromGet, stringIsEmpty) {
    $("#request-list").html("").hide();
    $("#friend-list").html("").hide();
    $("#other-list").html("").hide();

    // To check how many results we get from the search
    var requests = 0, friends = 0, others = 0;

    while (dataFromGet[0][requests]) {
        requests++;
    }
    while (dataFromGet[1][friends]) {
        friends++;
    }
    while (dataFromGet[2][others]) {
        others++;
    }
    if (requests == 0 && friends == 0 && others == 0) {
        $("#friend-list").append('<div class="Friend jumbotron">' + '<p>No users found!</p></div></a>');
    }

    else {
        if (requests != 0) {
            $("#request-list").append('<h2 class="my-playgrounds">Requests</h2>');
            for (var user in dataFromGet[0]) {
                $("#request-list").append('<a href="UserPage/?username=' + dataFromGet[0][user].Username + '">'
                    + '<div class="Friend jumbotron">'
                    + '<div class="friend-pic" >'
                    + '<span class="FriendPic">'
                    + '<img src="' + dataFromGet[0][user].ProfilePic.Path + '" alt="Photo of a user" width="200" class="img-responsive" />'
                    + '</span>'
                    + '</div>'
                    + '<div class="friend-info" >'
                    + '<span class="FriendName">'
                    + dataFromGet[0][user].Name + '</span>'
                    + '</div></div></a>'
                    + '<div class="request-buttons">'
                    + '<button id="remove-' + dataFromGet[0][user].id + '" class="accept-btn">Accept</button>'
                    + '<button id="accept-' + dataFromGet[0][user].id + '" class="remove-btn">Decline</button>'
                    + '</div>'
                    + '<form method="post" id="user-form">'
                    + '<input type="hidden" value="' + dataFromGet[0][user].Id + '" name="user-id" id="user-id" />'
                    + '</form>');
                $(".accept-btn").click(acceptFriend);
                $(".remove-btn").click(removeFriend);
            }
        }
        if (friends != 0) {
            $("#friend-list").append('<h2 class="my-playgrounds">Friends</h2>');
            for (var user in dataFromGet[1]) {
                $("#friend-list").append('<a href="UserPage/?username=' + dataFromGet[1][user].Username + '">'
                    + '<div class="Friend jumbotron">'
                    + '<div class="friend-pic" >'
                    + '<span class="FriendPic">'
                    + '<img src="' + dataFromGet[1][user].ProfilePic.Path + '" alt="Photo of a user" width="200" class="img-responsive" />'
                    + '</span>'
                    + '</div>'
                    + '<div class="friend-info" >'
                    + '<span class="FriendName">'
                    + dataFromGet[1][user].Name + '</span>'
                    + '<span class="ActivePlaygroundName">'
                    + dataFromGet[1][user].ActivePlaygroundName
                    + '</span></div></div></a>');
            }
        }
        if (others != 0 && !stringIsEmpty) {
            $("#other-list").append('<h2 class="my-playgrounds">Strangers</h2>');
            for (var user in dataFromGet[2]) {
                $("#other-list").append('<a href="UserPage/?username=' + dataFromGet[2][user].Username + '">'
                    + '<div class="Friend jumbotron">'
                    + '<div class="friend-pic" >'
                    + '<span class="FriendPic">'
                    + '<img src="' + dataFromGet[2][user].ProfilePic.Path + '" alt="Photo of a user" width="200" class="img-responsive" />'
                    + '</span>'
                    + '</div>'
                    + '<div class="friend-info" >'
                    + '<span class="FriendName">'
                    + dataFromGet[2][user].Name + '</span>'
                    + '</div></div></a>');
            }
        }    
    }
    $("#request-list").show();
    $("#friend-list").show();
    $("#other-list").show();
};

$("#search").on('input', function () {
    var text = $("#search").serialize();
    $.ajax({
        type: 'POST',
        url: "/Home/SearchUsersJson",
        data: text,
        dataType: 'json',
        success: function (dataFromGet) {
            if (text == "search=") {
                UpdateFeedFriend(dataFromGet, true)
            }
            else {
                UpdateFeedFriend(dataFromGet, false)
            }
        }
    });
});
