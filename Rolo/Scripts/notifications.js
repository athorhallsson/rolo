﻿$(document).ready(function () {
    CheckForNewFeedJson();
    CheckForNewFriends();
    setInterval(CheckForNewFriends, 5000);
    setInterval(CheckForNewFeedJson, 5000);
});

function CheckForNewFriends() {
    $.ajax({
        type: 'GET',
        url: "/Home/FriendNotificationJson",
        data: {},
        dataType: 'json',
        success: function (data) {
            if (data > 0) {
                $("#friend-notification-number-l").html(data).show();
                $("#friend-notification-number-m").html(data).show();
                $("#friend-notification-number-s").html(data).show();
                if (window.location.pathname == "/Home/Friend") {
                    $("#friends").css('background-position', '0px -1300px');
                    $("#friends-mobile").css('background-position', '0px -975px');
                    $("#friends-mobile-small").css('background-position', '0px -845px');
                }
                else {
                    $("#friends").css('background-position', '0px -600px');
                    $("#friends-mobile").css('background-position', '0px -450px');
                    $("#friends-mobile-small").css('background-position', '0px -390px');
                }
                
            }
            else {
                $("#friend-notification-number-l").hide();
                $("#friend-notification-number-m").hide();
                $("#friend-notification-number-s").hide();
                if (window.location.pathname == "/Home/Friend") {
                    $("#friends").css('background-position', '0px -1000px');
                    $("#friends-mobile").css('background-position', '0px -760spx');
                    $("#friends-mobile-small").css('background-position', '0px -650px');
                }
                else {
                    $("#friends").css('background-position', '0px -300px');
                    $("#friends-mobile").css('background-position', '0px -225px');
                    $("#friends-mobile-small").css('background-position', '0px -195px');
                }

                
            }
            
        }
    });
};

function CheckForNewFeedJson() {
    $.ajax({
        type: 'GET',
        url: "/Home/FeedNotificationJson",
        data: {},
        dataType: 'json',
        success: function (data) {
            if (data > 0) {
                $("#feed-notification-number-l").html(data).show();
                $("#feed-notification-number-m").html(data).show();
                $("#feed-notification-number-s").html(data).show();
                if (window.location.pathname == "/Home/Feed") {
                    $("#feed").css('background-position', '0px -1200px');
                    $("#feed-mobile").css('background-position', '0px -900px');
                    $("#feed-mobile-small").css('background-position', '0px -780px');
                }
                else {
                    $("#feed").css('background-position', '0px -500px');
                    $("#feed-mobile").css('background-position', '0px -375px');
                    $("#feed-mobile-small").css('background-position', '0px -325px');
                }
            }
            else {
                $("#feed-notification-number-l").hide();
                $("#feed-notification-number-m").hide();
                $("#feed-notification-number-s").hide();
                if (window.location.pathname == "/Home/Feed") {
                    $("#feed").css('background-position', '0px -900px');
                    $("#feed-mobile").css('background-position', '0px -675px');
                    $("#feed-mobile-small").css('background-position', '0px -585px');
                }
                else {
                    $("#feed").css('background-position', '0px -200px');
                    $("#feed-mobile").css('background-position', '0px -150px');
                    $("#feed-mobile-small").css('background-position', '0px -130px');
                }
                
            }
        }
    });
};