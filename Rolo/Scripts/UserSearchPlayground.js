﻿var position;

$(document).ready(function () {
    if (navigator.geolocation) {
        var pos = navigator.geolocation.getCurrentPosition(savePosition);
    } else {
        $("#nearby-playgrounds").append("<p>Geolocation is not supported by this browser.</p>");
    }
});
function savePosition(pos) {
    position = pos;
}

var UpdatePlaygrounds = function (dataFromGet, stringIsEmpty) {
    $("#my-playgrounds").html("").hide();
    $("#nearby-playgrounds").html("").hide();

    var myCount = 0, nearbyCount = 0;

    while (dataFromGet[0][myCount]) {
        myCount++;
    }
    while (dataFromGet[1][nearbyCount]) {
        nearbyCount++;
    }

    if (myCount == 0 && nearbyCount == 0) {
        $("#nearby-playgrounds").append('<div class="Friend jumbotron">' + '<p>No playgrounds found!</p></div></a>');
    }
    else {
        if (myCount != 0) {
            $("#my-playgrounds").append('<h2 class="my-playgrounds">My Playgrounds</h2>');
            for (var playground in dataFromGet[0]) {
                var range = dataFromGet[0][playground].Range
                if (range > 1000) {
                    range = (range / 1000.0).toFixed(1);
                    range = range + " km";
                }
                else {
                    range = ((range / 10).toFixed(0)) * 10;
                    range = range + " m";
                }

                $("#nearby-playgrounds").append('<a href="/Home/PlaygroundInfo/' + dataFromGet[0][playground].Id + '">'
                    + '<div class="jumbotron" class="guest-playground-list-item">'
                    + '<p class="guest-playground-name">' + dataFromGet[0][playground].Name + '</p>'
                    // + '<p class="guest-playground-range">' + range + "</p></div></a>");
                    + "</div></a>");
            }
        }
        if (nearbyCount != 0) {
            $("#nearby-playgrounds").append('<div class="my-playgrounds"><h2>Nearby Playgrounds</h2></div>');
            for (var playground in dataFromGet[1]) {
                var range = dataFromGet[1][playground].Range
                if (range > 1000) {
                    range = (range / 1000.0).toFixed(1);
                    range = range + " km";
                }
                else {
                    range = ((range / 10).toFixed(0)) * 10;
                    range = range + " m";
                }

                $("#nearby-playgrounds").append('<a href="/Home/PlaygroundInfo/' + dataFromGet[1][playground].Id + '">'
                    + '<div class="jumbotron" class="guest-playground-list-item">'
                    + '<p class="guest-playground-name">' + dataFromGet[1][playground].Name + '</p>'
                    + '<p class="guest-playground-range">' + range + "</p></div></a>");                   
            }
        }
    }
    $("#my-playgrounds").show();
    $("#nearby-playgrounds").show();
}

$("#search").on('input', function () {

    var posLatitude = position.coords.latitude;
    var posLongitude = position.coords.longitude;

    var text = $("#search").val();
    $.ajax({
        type: 'POST',
        url: "/Home/UserSearchPlaygroundsJson",
        data: { lat: posLatitude, lon: posLongitude, search: text },
        dataType: 'json',
        success: function (dataFromGet) {
            if (text == "") {
                UpdatePlaygrounds(dataFromGet, true)
            }
            else {
                UpdatePlaygrounds(dataFromGet, false)
            }
        }
    });
});