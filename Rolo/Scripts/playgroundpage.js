$(document).ready(function () {
    ShowFeed();
    ImageShow();

    $(".rateinfo").click(function () {
        var rateform = $("#rate-form").serialize();
        $.ajax({
            type: 'POST',
            url: "/Home/RatePlayground/",
            data: rateform,
            dataType: 'json',
            success: function (data) {
                document.getElementById("average-rating").innerHTML = "Rating " + data + "/5";
            },
            error: function () {
                alert("Sorry, you can not rate at this time.");
            }
        });
    });

    $("#follow-btn").click(function () {
        var form = $("#playground-form").serialize();
        $.ajax({
            type: 'POST',
            url: "/Home/FollowPlaygroundJson/",
            data: form,
            dataType: 'json',
            success: function () {
                var button = document.getElementById("follow-btn");
                if (button.innerText == "Follow") {
                    button.innerHTML = "Unfollow";
                }
                else {
                    button.innerHTML = "Follow";
                }
            },
            error: function () {
                alert("Error");
            }
        });
    });

    $("#check-in-btn").click(function () {
        var form = $("#playground-form").serialize();
        $.ajax({
            type: 'POST',
            url: "/Home/CheckInPlaygroundJson/",
            data: form,
            dataType: 'json',
            success: function () {
                var button = document.getElementById("check-in-btn");
                if ($(button).hasClass("checked-in"))
                {
                    $(button).removeClass("checked-in");
                }
                else
                {
                    $(button).addClass("checked-in");
                }
            },
            error: function () {
                alert("Error");
            }
        });
    });

});

var loadPlaygroundMap = function () {
    var latitude = parseFloat($("#playground-location-lat").val().replace(/,/g, '.'));
    var longitude = parseFloat($("#playground-location-lon").val().replace(/,/g, '.'));
    var map = new GMaps({
        el: '#map',
        lat: latitude,
        lng: longitude
    });
    map.addMarker({
        lat: latitude,
        lng: longitude,
        icon: '/Data/Location_Black.png'
    });
};

var MapShow = function () {
    $(".playground-image").css({
        "position": "absolute",
        "left": "-10000px"
    });

    $(".map-div").css({
        "position": "relative",
        "left": "0px"
    });

    $("#map").css("display", "block");

    loadPlaygroundMap();
    $("#map-btn").css("background-position", "0px -150px");
    $("#map-btn").click(ImageShow);
};

var ImageShow = function () {
    $(".map-div").css({
        "position": "absolute",
        "left": "-10000px"
    });
    $(".playground-image").css({
        "position": "relative",
        "left": "0px"
    });
    $("#map-btn").css("background-position", "0px -100px");
    $("#map-btn").click(MapShow);
};

var ShowFeatures = function () {
    $("#playground-features-section").show();
    $("#playground-feed-section").hide();
    $("#playground-feature-btn").css("background-position", "0px -450px");
    $("#playground-feed-btn").css("background-position", "0px -300px");
    $("#playground-feed-btn").click(ShowFeed);
};

var ShowFeed = function () {
    $("#playground-features-section").hide();
    $("#playground-feed-section").show();
    $("#playground-feature-btn").css("background-position", "0px -400px");
    $("#playground-feed-btn").css("background-position", "0px -350px");
    $("#playground-feature-btn").click(ShowFeatures);
};