﻿$(document).ready(function () {
    getLocation();
    ListShow();

    $("#playgrounds-mobile-small").css('background-position', '0px -520px');
    $("#playgrounds-mobile").css('background-position', '0px -600px');
    $("#playgrounds").css('background-position', '0px -800px');
});

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else { 
        $("#nearby-playgrounds").append("<p>Geolocation is not supported by this browser.</p>");
    }
}

var data;

function showPosition(position) {

    var posLatitude = position.coords.latitude;
    var posLongitude = position.coords.longitude;

    $("#nearby-playgrounds").append('<h2 class="loading">Loading nearby playgrounds..</h2>')
    $.ajax({
        type: "POST",
        url: "/Home/GuestPlaygroundListJson/",
        data: { lat: posLatitude, lon: posLongitude },
        dataType: 'json',
        success: function (dataFromGet) {
            $("#nearby-playgrounds").html('<h2 class="my-playgrounds">Nearby Playgrounds</h2>');
            
            for (var playground in dataFromGet) {
                var range = dataFromGet[playground].Range
                if (range > 1000) {
                    range = (range / 1000.0).toFixed(1);
                    range = range + " km";
                }
                else {
                    range = ((range / 10).toFixed(0)) * 10;
                    range = range + " m";
                }

                $("#nearby-playgrounds").append('<a href="/Home/PlaygroundInfo/' + dataFromGet[playground].Id +
                    '"><div class="jumbotron" class="guest-playground-list-item"><div class="guest-nearby-play"><p class="guest-playground-name">'
                    + dataFromGet[playground].Name + '</p><p class="guest-playground-range">'
                    + range + "</p></div></div></a>");
            }

            data = dataFromGet;
        }
    });
}

var ListShow = function () {
    $(".guest-playground-list").show();
    $(".user-playground-map-div").css({
        "position": "absolute",
        "left": "-10000px"
    });
    $(".guest-map-btn button").css("background-position", "0px -100px");
    $(".guest-map-btn button").click(MapShow);
};

var MapShow = function () {
    $(".guest-playground-list").hide();
    $(".user-playground-map-div").css({
        "position": "relative",
        "left": "0px"
    });

    loadMap(data);

    $(".guest-map-btn button").css("background-position", "0px -150px");
    $(".guest-map-btn button").click(ListShow);
};

function loadMap(data) {
    var map = new GMaps({
        div: '#interactive-map',
        lat: position.coords.latitude,
        lng: position.coords.longitude,
        zoom: 14
    });
    for (var playground in data) {
        var iconImage = '/Data/Location_Black.png';
        if (data[playground].ActiveFriends) {
            iconImage = '/Data/Location_Green.png';
        }
        else if (data[playground].CurrentUserIsCheckedIn) {
            iconImage = '/Data/Location_Black.png';
        }
        else if (!data[playground].Empty) {
            iconImage = '/Data/Location_Black.png';
        }
        map.addMarker({
            lat: data[playground].Latitude,
            lng: data[playground].Longitude,
            title: data[playground].Name,
            icon: iconImage,
            infoWindow: {
                content: '<p>' + data[playground].Name + '</p>' + '<a href="/Home/Playgroundinfo/' + data[playground].Id + '">More</a>'
            }
        });
    }

}