function remove() {
    var form = $("#user-form").serialize();
    $.ajax({
        type: 'POST',
        url: "/Home/RemoveFriendJson/",
        data: form,
        dataType: 'json',
        success: function () {
            var addButton = $('<button id="add-friend-btn"/>').text('Add Friend').click(function () { add(); this.remove()});
            $(".container section .profilePic").append(addButton);
            $("#cancel-friend-btn").remove();
            $("#remove-friend-btn").remove();
        },
        error: function () {
            alert("An error occured. Please try again later.");
        }
    });
};

function add() {
    var form = $("#user-form").serialize();
    $.ajax({
        type: 'POST',
        url: "/Home/AddFriendJson/",
        data: form,
        dataType: 'json',
        success: function () {
            var removeButton = $('<button id="cancel-friend-btn"/>').text('Cancel Friend Request').click(function () { remove(); this.remove() });
            $(".container section .profilePic").append(removeButton);
            $("#add-friend-btn").click(add).remove();
        },
        error: function () {
            alert("An error occured. Please try again later.");
        }
    });
};

$(function () {
    $("#cancel-friend-btn").on("click", function () { remove(); this.remove(); });
    $("#remove-friend-btn").on("click", function () { remove(); this.remove(); });
    $("#add-friend-btn").on("click", function () { add(); this.remove(); });
});