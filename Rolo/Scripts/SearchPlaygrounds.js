﻿var position;

$(document).ready(function () {
    if (navigator.geolocation) {
        var pos = navigator.geolocation.getCurrentPosition(savePosition);
    } else {
        $("#nearby-playgrounds").append("<p>Geolocation is not supported by this browser.</p>");
    }
});

function savePosition(pos) {
    position = pos;
}

var UpdatePlaygrounds = function (dataFromGet, stringIsEmpty) {
    $("#nearby-playgrounds").html("").hide();

    var count = 0;

    while (dataFromGet[count]) {
        count++;
    }

    if (count == 0) {
        $("#nearby-playgrounds").append('<div class="Friend jumbotron">' + '<p>No playgrounds found!</p></div></a>');
    }
    else {
        $("#nearby-playgrounds").append('<h2>Nearby playgrounds</h2>');
        for (var playground in dataFromGet) {
            var range = dataFromGet[playground].Range
            if (range > 1000) {
                range = (range / 1000.0).toFixed(1);
                range = range + " km";
            }
            else {
                range = ((range / 10).toFixed(0)) * 10;
                range = range + " m";
            }

            $("#nearby-playgrounds").append('<a href="/Home/PlaygroundInfo/' + dataFromGet[playground].Id + '">'
                + '<div class="jumbotron" class="guest-playground-list-item">' 
                + '<p class="guest-playground-name">' + dataFromGet[playground].Name + '</p>'
                + '<p class="guest-playground-range">' + range + "</p></div></a>");
        }
    }
    $("#nearby-playgrounds").show();
}

$("#search").on('input', function () {

    var posLatitude = position.coords.latitude;
    var posLongitude = position.coords.longitude;

    var text = $("#search").val();
    $.ajax({
        type: 'POST',
        url: "/Home/GuestSearchPlaygroundsJson",
        data: { lat: posLatitude, lon: posLongitude, search: text },
        dataType: 'json',
        success: function (dataFromGet) {
            if (text == "") {
                UpdatePlaygrounds(dataFromGet, true)
            }
            else {
                UpdatePlaygrounds(dataFromGet, false)
            }            
        }
    });
});