﻿
var UpdatePlaygroundFeed = function (data) {
    $("#new-playground-post").val("");
    $("#comment-input").val("");
    $("#playground-feed").html("").hide();

    for (var post in data) {
        var jsonDate = data[post].DateModified;
        var postDate = new Date(parseInt(jsonDate.substr(6)));
        postDate = $.timeago(postDate);

        $("#playground-feed").append('<div class="jumbotron playground-feed"><div class="wrap1"><div class="userinfo"><img src="'
            + data[post].Author.ProfilePic.Path + '" alt="ProfilePic" id="ProfilePic"/><p class="author">'
            + data[post].Author.Name + '</p><p class="date-to-timeago" title="' + postDate + '">' + postDate + '</p></div></div><div class="wrap2"><div class="comment-text"><p>'
            + data[post].Text + '</p><button id="' + data[post].Id + '" class="comment-btn">Comment</button><div id="div'
            + data[post].Id + '" class="hidden-comment-form" style="display: none;"><form id="form' + data[post].Id + '" class="comment-form"><input type="hidden" name="post-id" value="'
            + data[post].Id + '"/><input type="hidden" value="' + data[post].PlaygroundId + '" id="playgr-id" name="playgr-id" /><input class="text-comment" type="text" placeholder="Comment.." name="comment-input" maxlength="100" autocomplete="off" /></form><button id="btn'
            + data[post].Id + '" class="post-comment"></button></div></div></div></div>');

        for (var comment in data[post].Comments) {
            var jsonDate = data[post].DateModified;
            var commentDate = new Date(parseInt(jsonDate.substr(6)));
            commentDate = $.timeago(commentDate);

            $("#playground-feed").append('<div class="comment-box"><div class="jumbotron comment"><div class="wrap1"><div class="userinfo"><img src="'
            + data[post].Comments[comment].Author.ProfilePic.Path + '" alt="profile-pic"/><p class="author">'
            + data[post].Comments[comment].Author.Name + '</p><p class="date-to-timeago" title="' + commentDate + '">' + commentDate + '</p></div></div><div class="wrap2"><div><p>'
            + data[post].Comments[comment].Text + '</p></div></div></div></div>');
        }
    }

    $("#playground-feed").fadeIn();
    $(".comment-btn").click(CommentClick);
};

var CommentClick = function (event) {
    var id = event.target.id;
    var divId = "#div" + id;
    var formId = "#form" + id;
    var commentBtnTd = "#btn" + id;
    id = "#" + id;

    $(divId).show();
    $(id).hide();

    $(commentBtnTd).click(function () {
        var commentText = $(formId).serialize();
        $.ajax({
            type: 'POST',
            url: "/Home/AddCommentToPostPlaygrJson",
            data: commentText,
            dataType: 'json',
            success: UpdatePlaygroundFeed
        });
    });
};

$("#playground-post-btn").click(function () { 
    if ($("#new-playground-post").val() != "") {
        var text = $("#playground-post-form").serialize();
        $.ajax({
            type: 'POST',
            url: "/Home/AddPostToPlaygroundJson",
            data: text,
            dataType: 'json',
            success: UpdatePlaygroundFeed,
            error: function (xhr, err) {
                alert("readyState: " + xhr.readyState + "\nstatus: " + xhr.status + "\nUSER POST");
                alert("responseText: " + xhr.responseText);
            }
        });
    }
});

$(document).ready(function () {
    $(".date-to-timeago").timeago();
    $(".comment-btn").click(CommentClick);
});

