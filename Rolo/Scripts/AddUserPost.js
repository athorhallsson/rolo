﻿
var UpdateFeed = function (dataFromGet) {
    $("#new-user-post").val("");
    $("#comment-input").val("");
    $("#post-feed").html("").hide();

    for (var post in dataFromGet) {
        var jsonDate = dataFromGet[post].DateModified;
        var postDate = new Date(parseInt(jsonDate.substr(6)));
        postDate = $.timeago(postDate);

        var playgroundName = "";
        if (dataFromGet[post].PlaygroundId != 0) {
            playgroundName = '<a href="/Home/PlaygroundInfo/' + dataFromGet[post].PlaygroundId + '">'
                               + '<p class="playground-name">' + dataFromGet[post].PlaygroundName + '</p>'
                               + '</a>';
        }

        $("#post-feed").append('<div class="jumbotron post">'
                                + '<div class="wrap1">'
                                    + '<div class="userinfo">'
                                        + '<img src="' + dataFromGet[post].Author.ProfilePic.Path + '" alt="ProfilePic"/>' 
                                        + '<p>' + dataFromGet[post].Author.Name + '</p>'                                        
                                        + '<p class="date-to-timeago" title="' + postDate + '">' + postDate + '</p>'
                                    + '</div>'
                                + '</div>'
                                    + '<div class="wrap2">'
                                        + '<div class="playground-name">'
                                            + '<div class="rolo-text">'
                                            + playgroundName
                                                + '<p>' + dataFromGet[post].Text + '</p>'
                                            + '</div>'
                                            + '<div class="rolo-btn">'
                                            + '<button id="' + dataFromGet[post].Id + '" class="comment-btn">Comment</button>'
                                        + '</div>'                                
                                    + '</div>' 
                                + '</div>'
                                + '<div id="div' + dataFromGet[post].Id + '" class="hidden-comment-form" style="display: none;">'
                                    + '<form id="form' + dataFromGet[post].Id + '" class="comment-form">'
                                        + '<input type="hidden" name="post-id" value="' + dataFromGet[post].Id + '"/>'
                                        + '<input type="text" placeholder="Comment.." name="comment-input" maxlength="100" autocomplete="off" />'
                                        + '<button id="btn' + dataFromGet[post].Id + '" class="post-comment"></button>'
                                    + '</form>'                                    
                                + '</div>'
                            + '</div>');

        for (var comment in dataFromGet[post].Comments) {
            var jsonDate = dataFromGet[post].DateModified;
            var commentDate = new Date(parseInt(jsonDate.substr(6)));
            commentDate = $.timeago(commentDate);

            $("#post-feed").append('<div class="comment-box"><div class="jumbotron comment"><div class="wrap1"><div class="userinfo"><img src="'
            + dataFromGet[post].Comments[comment].Author.ProfilePic.Path + '" alt="profile-pic"/><p>'
            + dataFromGet[post].Comments[comment].Author.Name + '</p><p class="date-to-timeago" title="' + commentDate + '">' + commentDate + '</p></div></div><div class="wrap2"><div class="comment-text"><p>'
            + dataFromGet[post].Comments[comment].Text + '</p></div></div></div></div>');
        }
    }

    $("#post-feed").fadeIn();
    $("#newestPost").val(new Date(parseInt(dataFromGet[0].DateModified.substr(6))));
    $(".comment-btn").click(CommentClick);
    UpdateLatestRead();
};

var CommentClick = function (event) {
    var id = event.target.id;
    var divId = "#div" + id;
    var formId = "#form" + id;
    var commentBtnTd = "#btn" + id;
    
    $(divId).show();
    $("#" + id).hide();

    $(commentBtnTd).click(function () {
        var commentText = $(formId).serialize();
        $.ajax({
            type: 'POST',
            url: "/Home/AddCommentToPostUserJson",
            data: commentText,
            dataType: 'json',
            success: UpdateFeed
        });
    })
};

var UpdateLatestRead = function () {
    $.ajax({
        type: 'POST',
        url: "/Home/UpdateLatestReadJson",
        data: {},
        dataType: 'json',
        success: {}
    });
};

$("#user-post-btn").click(function () {
    if ($("#new-user-post").val() != ""){
        var text = $("#user-post-form").serialize();
        $.ajax({
            type: 'POST',
            url: "/Home/AddPostToUserJson",
            data: text,
            dataType: 'json',
            success: UpdateFeed,
            error: function (xhr, err) {
                alert("readyState: " + xhr.readyState + "\nstatus: " + xhr.status + "\nUSER POST");
                alert("responseText: " + xhr.responseText);
            }
        });
    }
});

$(document).ready(function () {
    $(".date-to-timeago").timeago();
    $(".comment-btn").click(CommentClick);
    UpdateLatestRead();

    $("#feed-mobile-small").css('background-position', '0px -585px');
    $("#feed-mobile").css('background-position', '0px -675px');
    $("#feed").css('background-position', '0px -900px');
});

