﻿$(document).ready(function () {
    getLocation();
    ListShow();

    $("#playgrounds-mobile-small").css('background-position', '0px -520px');
    $("#playgrounds-mobile").css('background-position', '0px -600px');
    $("#playgrounds").css('background-position', '0px -800px');
});

var ListShow = function () {
    $("#user-playground-list").show();
    $(".user-playground-map-div").css({
        "position": "absolute",
        "left": "-10000px"
    });
    $("#user-playground-map-btn button").css("background-position", "0px 0px");
    $("#user-playground-map-btn button").click(MapShow);
};

var MapShow = function () {
    $("#user-playground-list").hide();
    $(".user-playground-map-div").css({
        "position": "relative",
        "left": "0px"
    });

    loadMap(data);

    $("#user-playground-map-btn button").css("background-position", "0px -455px");
    $("#user-playground-map-btn button").click(ListShow);
};

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        $("#nearby-playgrounds").append("<p>Geolocation is not supported by this browser.</p>");
    }
}

var data;

function showPosition(position) {
    var posLatitude = position.coords.latitude;
    var posLongitude = position.coords.longitude;

    $("#nearby-playgrounds").append('<h2 class="loading">Loading nearby playgrounds..</h2>');
    $.ajax({
        type: "POST",
        url: "/Home/UserPlaygroundListJson/",
        data: { lat: posLatitude, lon: posLongitude },
        dataType: 'json',
        success: function (dataFromGet) {
            data = dataFromGet;
            $("#nearby-playgrounds").html('<h2 class="my-playgrounds">Nearby Playgrounds</h2>');
            for (var playground in dataFromGet) {
                var range = dataFromGet[playground].Range;
                if (range > 1000) {
                    range = (range / 1000.0).toFixed(1);
                    range = range + " km";
                }
                else {
                    range = ((range / 10).toFixed(0)) * 10;
                    range = range + " m";
                }

                var iconImage;

                if (dataFromGet[playground].ActiveFriends) {
                    iconImage = '/Data/LocationRingGreen.png';
                }
                else if (dataFromGet[playground].CurrentUserIsCheckedIn) {
                    iconImage = '/Data/Location_White.png';
                }
                else if (!dataFromGet[playground].Empty) {
                    iconImage = '/Data/LocationRingOrange.png';
                }
                else {
                    iconImage = '/Data/LocationRingRed.png';
                }

                $("#nearby-playgrounds").append('<a href="/Home/PlaygroundInfo/' + dataFromGet[playground].Id +
                    '"><div class="jumbotron" class="user-playground-list-item"><div class="play-status"><img src="' + iconImage + '" /><p class="user-playground-name">'
                    + dataFromGet[playground].Name + '</p><p class="user-playground-range">'
                    + range + '</p></div></div></a>');
            }
            loadMap(dataFromGet);
        },
        error: function (xhr, err) {
        }

    });
}

function loadMap(data) {
    var map = new GMaps({
        div: '#interactive-map',
        lat: position.coords.latitude,
        lng: position.coords.longitude,
        zoom: 14
    });
    for (var playground in data) {
        var iconImage = '/Data/Location_Red.png';
        if (data[playground].CurrentUserIsCheckedIn) {
            iconImage = '/Data/Location_White.png';
        }
        else if (data[playground].ActiveFriends) {
            iconImage = '/Data/Location_LtGreen.png';
        }
        else if (!data[playground].Empty) {
            iconImage = '/Data/Location_LtYellow.png';
        }
        map.addMarker({
            lat: data[playground].Latitude,
            lng: data[playground].Longitude,
            title: data[playground].Name,
            icon: iconImage,
            infoWindow: {
                content: '<p>' + data[playground].Name + '</p>' + '<a href="/Home/Playgroundinfo/' + data[playground].Id + '">More</a>'
            }
        });
    }

}